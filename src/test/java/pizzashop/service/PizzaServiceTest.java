package pizzashop.service;

import org.junit.jupiter.api.*;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import pizzashop.exceptions.WrongInputsException;
import pizzashop.model.Payment;
import pizzashop.model.PaymentType;
import pizzashop.repository.MenuRepository;
import pizzashop.repository.PaymentRepository;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.times;

class PizzaServiceTest {

    private PizzaService pizzaService;

    @Mock
    private MenuRepository menuRepositoryMockito;

    @Mock
    private PaymentRepository paymentRepositoryMockito;

    private PizzaService pizzaServiceMockito;

    private static String TABLE_ERROR = "Table number should be an int between 1 and 8 ";
    private static String AMOUNT_ERROR = "Amount should be a positive real number ";
    private static final String TYPE_ERROR = "Type should be either Cash or Card ";

    @BeforeEach
    void setUp() {
        this.pizzaService = new PizzaService(new MenuRepository(), new PaymentRepository());
        pizzaServiceMockito = Mockito.spy(new PizzaService(new MenuRepository(), new PaymentRepository()));
    }

    @AfterEach
    void tearDown() {
    }

    @AfterAll
    static void resetWorkspace() {
        TABLE_ERROR = "";
        AMOUNT_ERROR = "";
    }


    @Test
    void addPayment_Valid_Added_Mockito() throws Exception{
        pizzaServiceMockito.paymentsCleanup(new ArrayList<>());
        int table = 1, size = 0;
        PaymentType paymentType = PaymentType.Card;
        double amount = 10.5;

        Payment p = new Payment(table,paymentType,amount);

        Mockito.when(pizzaServiceMockito.getPayments()).thenReturn(Collections.singletonList(p));
        Mockito.doNothing().when(pizzaServiceMockito).addPayment(table,paymentType,amount);

        assertEquals(1, pizzaServiceMockito.getPayments().size());
        assert 1 == pizzaServiceMockito.getPayments().size();
        Mockito.verify(pizzaServiceMockito, times(2)).getPayments();
    }

    @Test
    void getTotalAmount_Valid_Mockito() throws WrongInputsException {
        //set-up
        pizzaServiceMockito.paymentsCleanup(new ArrayList<>());
        double total=0;
        //act
        Mockito.when(pizzaServiceMockito.getTotalAmount(PaymentType.Card)).thenReturn(total);


        //assert
        assertEquals(0, total);
        Mockito.verify(pizzaServiceMockito, times(1)).getTotalAmount(PaymentType.Card);
    }

    @Test
    void addPayment_Valid_Added() throws Exception {
        //set-up
        int table = 1, size = 0;
        PaymentType paymentType = PaymentType.Card;
        double amount = 10.5;

        if (this.pizzaService.getPayments() != null)
            size = this.pizzaService.getPayments().size();

        //act
        this.pizzaService.addPayment(table, paymentType, amount);

        //assert
        assertEquals(size + 1, pizzaService.getPayments().size());
    }

    @Test
    @Order(1)
    void addPayment_Invalid_WrongTable1NotAdded() {

        //set-up
        int table = -1;
        PaymentType paymentType = PaymentType.Card;
        double amount = 10.5;

        //act
        Exception thrown = Assertions.assertThrows(Exception.class, () -> {
            //Code under test
            this.pizzaService.addPayment(table, paymentType, amount);
            throw new Exception(TABLE_ERROR);
        });
        Assertions.assertEquals(TABLE_ERROR, thrown.getMessage());
    }

    @Test
    @Order(2)
    void addPayment_Invalid_WrongTable2NotAdded() {

        //set-up
        int table = 9;
        PaymentType paymentType = PaymentType.Card;
        double amount = 10.5;

        //act
        Exception thrown = Assertions.assertThrows(Exception.class, () -> {
            //Code under test
            this.pizzaService.addPayment(table, paymentType, amount);

        });
        Assertions.assertEquals(TABLE_ERROR, thrown.getMessage());
    }

    @Test
    void addPayment_Invalid_WrongAmountNotAdded() {
        //set-up
        int table = 1;
        PaymentType paymentType = PaymentType.Card;
        double amount = -10.5;

        //act
        Exception thrown = Assertions.assertThrows(Exception.class, () -> {
            //Code under test
            this.pizzaService.addPayment(table, paymentType, amount);

        });
        Assertions.assertEquals(AMOUNT_ERROR, thrown.getMessage());
    }

    @Test
    void addPayment_Invalid_TableLessThan() {
        //set-up
        int table = 0;
        PaymentType paymentType = PaymentType.Card;
        double amount = 10.5;

        //act
        Exception thrown = Assertions.assertThrows(Exception.class, () -> {
            //Code under test
            this.pizzaService.addPayment(table, paymentType, amount);

        });
        //assert
        Assertions.assertEquals(TABLE_ERROR, thrown.getMessage());
    }

    @Test
    void addPayment_Valid_TableInRange1() throws Exception {
        //set-up
        int table = 1, size = 0;
        PaymentType paymentType = PaymentType.Card;
        double amount = 10.5;

        if (this.pizzaService.getPayments() != null)
            size = this.pizzaService.getPayments().size();

        //act
        this.pizzaService.addPayment(table, paymentType, amount);

        //assert
        assertEquals(size + 1, pizzaService.getPayments().size());
    }

    @Test
    void addPayment_Valid_TableInRange2() throws Exception {
        //set-up
        int table = 2, size = 0;
        PaymentType paymentType = PaymentType.Card;
        double amount = 10.5;

        if (this.pizzaService.getPayments() != null)
            size = this.pizzaService.getPayments().size();

        //act
        this.pizzaService.addPayment(table, paymentType, amount);

        //assert
        assertEquals(size + 1, pizzaService.getPayments().size());
    }

    @Test
    void addPayment_Valid_TableInRange7() throws Exception {
        //set-up
        int table = 7, size = 0;
        PaymentType paymentType = PaymentType.Card;
        double amount = 10.5;

        if (this.pizzaService.getPayments() != null)
            size = this.pizzaService.getPayments().size();

        //act
        this.pizzaService.addPayment(table, paymentType, amount);

        //assert
        assertEquals(size + 1, pizzaService.getPayments().size());
    }

    @Test
    void addPayment_Valid_TableInRange8() throws Exception {
        //set-up
        int table = 8, size = 0;
        PaymentType paymentType = PaymentType.Card;
        double amount = 10.5;

        if (this.pizzaService.getPayments() != null)
            size = this.pizzaService.getPayments().size();

        //act
        this.pizzaService.addPayment(table, paymentType, amount);

        //assert
        assertEquals(size + 1, pizzaService.getPayments().size());
    }

    @Test
    void addPayment_Invalid_TableGreaterThan() {
        //set-up
        int table = 9;
        PaymentType paymentType = PaymentType.Card;
        double amount = 10.5;

        //act
        Exception thrown = Assertions.assertThrows(Exception.class, () -> {
            //Code under test
            this.pizzaService.addPayment(table, paymentType, amount);

        });
        //assert
        Assertions.assertEquals(TABLE_ERROR, thrown.getMessage());
    }

    @Test
    @DisplayName("Amount less than 0.")
    void addPayment_Invalid_AmountLessThan() {
        //set-up
        int table = 1;
        PaymentType paymentType = PaymentType.Card;
        double amount = 0;

        //act
        Exception thrown = Assertions.assertThrows(Exception.class, () -> {
            //Code under test
            this.pizzaService.addPayment(table, paymentType, amount);

        });
        Assertions.assertEquals(AMOUNT_ERROR, thrown.getMessage());
    }

    @Test
    void addPayment_Valid_AmountInRange01() throws Exception {
        //set-up
        int table = 1, size = 0;
        PaymentType paymentType = PaymentType.Card;
        double amount = 0.1;

        if (this.pizzaService.getPayments() != null)
            size = this.pizzaService.getPayments().size();

        //act
        this.pizzaService.addPayment(table, paymentType, amount);

        //assert
        assertEquals(size + 1, pizzaService.getPayments().size());
    }

    @Test
    void addPayment_Valid_AmountInRange1() throws Exception {
        //set-up
        int table = 1, size = 0;
        PaymentType paymentType = PaymentType.Card;
        double amount = 1;

        if (this.pizzaService.getPayments() != null)
            size = this.pizzaService.getPayments().size();

        //act
        this.pizzaService.addPayment(table, paymentType, amount);

        //assert
        assertEquals(size + 1, pizzaService.getPayments().size());
    }

    @Test
    void addPayment_Valid_AmountInRangeMAXMinus1() throws Exception {
        //set-up
        int table = 1, size = 0;
        PaymentType paymentType = PaymentType.Card;
        double amount = Double.MAX_VALUE - 1;

        if (this.pizzaService.getPayments() != null)
            size = this.pizzaService.getPayments().size();

        //act
        this.pizzaService.addPayment(table, paymentType, amount);

        //assert
        assertEquals(size + 1, pizzaService.getPayments().size());
    }

    @Test
    void addPayment_Valid_AmountInRangeMAX() throws Exception {
        //set-up
        int table = 1, size = 0;
        PaymentType paymentType = PaymentType.Card;
        double amount = Double.MAX_VALUE;

        if (this.pizzaService.getPayments() != null)
            size = this.pizzaService.getPayments().size();

        //act
        this.pizzaService.addPayment(table, paymentType, amount);

        //assert
        assertEquals(size + 1, pizzaService.getPayments().size());
    }

    @Test
    void getTotalAmount_Valid_1() throws WrongInputsException {
        //set-up
        this.pizzaService.paymentsCleanup(null);

        //act
        double total = pizzaService.getTotalAmount(PaymentType.Card);

        //assert
        assertEquals(0, total);
    }

    @Test
    void getTotalAmount_Valid_2() throws WrongInputsException {
        //set-up
        this.pizzaService.paymentsCleanup(new ArrayList<>());

        //act
        double total = pizzaService.getTotalAmount(PaymentType.Card);

        //assert
        assertEquals(0, total);
    }


    @Test
    void getTotalAmount_Valid_4() throws WrongInputsException {
        //set-up
        Payment p1 = new Payment(1,null,10.5);
        List<Payment> paymentList= new ArrayList<>();
        paymentList.add(p1);
        this.pizzaService.paymentsCleanup(paymentList);

        //act
        double total = pizzaService.getTotalAmount(PaymentType.Card);

        //assert
        assertEquals(0, total);
    }

    @Test
    void getTotalAmount_Valid_5() throws WrongInputsException {
        //set-up
        Payment p1 = new Payment(1,PaymentType.Card,10.5);
        List<Payment> paymentList= new ArrayList<>();
        paymentList.add(p1);
        this.pizzaService.paymentsCleanup(paymentList);

        //act
        double total = pizzaService.getTotalAmount(PaymentType.Cash);

        //assert
        assertEquals(0, total);
    }

    @Test
    void getTotalAmount_Valid_6() throws WrongInputsException {
        //set-up

        Payment p1 = new Payment(1,PaymentType.Card,10.5);
        Payment p2 = new Payment(1,PaymentType.Card,10.5);
        List<Payment> paymentList= new ArrayList<>();
        paymentList.add(p1);
        paymentList.add(p2);
        this.pizzaService.paymentsCleanup(paymentList);

        //act
        double total = pizzaService.getTotalAmount(PaymentType.Card);

        //assert
        assertEquals(p2.getAmount() + p1.getAmount(), total);
    }

    @Test
    void getTotalAmount_Valid_7() throws WrongInputsException {
        //set-up
        Payment p1 = new Payment(1,PaymentType.Card,10.5);
        Payment p2 = new Payment(1,PaymentType.Cash,10.5);
        List<Payment> paymentList= new ArrayList<>();
        paymentList.add(p1);
        paymentList.add(p2);
        this.pizzaService.paymentsCleanup(paymentList);

        //act
        double total = pizzaService.getTotalAmount(PaymentType.Card);

        //assert
        assertEquals(p1.getAmount() , total);
    }

    @Test
    void getTotalAmount_Invalid() {
        //set-up
        Payment p1 = new Payment(1, null, 10.5);
        List<Payment> paymentList= new ArrayList<>();
        paymentList.add(p1);
        this.pizzaService.paymentsCleanup(paymentList);

        //act
        Exception thrown = Assertions.assertThrows(Exception.class, () -> {
            //Code under test
            this.pizzaService.getTotalAmount(null);
        });

        //assert
        Assertions.assertEquals(TYPE_ERROR, thrown.getMessage());
    }
}