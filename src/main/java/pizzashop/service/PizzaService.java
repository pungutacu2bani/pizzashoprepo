package pizzashop.service;

import pizzashop.exceptions.WrongInputsException;
import pizzashop.model.MenuDataModel;
import pizzashop.model.Payment;
import pizzashop.model.PaymentType;
import pizzashop.repository.MenuRepository;
import pizzashop.repository.PaymentRepository;

import java.util.List;

public class PizzaService {

    private final MenuRepository menuRepo;
    private final PaymentRepository payRepo;
    public static final String TABLE_ERROR = "Table number should be an int between 1 and 8 ";
    public static final String TYPE_ERROR = "Type should be either Cash or Card ";
    public static final String AMOUNT_ERROR = "Amount should be a positive real number ";

    public PizzaService(MenuRepository menuRepo, PaymentRepository payRepo) {
        this.menuRepo = menuRepo;
        this.payRepo = payRepo;
    }

    public List<MenuDataModel> getMenuData() {
        return menuRepo.getMenu();
    }

    public List<Payment> getPayments() {
        return payRepo.getAll();
    }

    public void paymentsCleanup(List<Payment> list) {
        payRepo.paymentsCleanup(list);
    }

    public void addPayment(int table, PaymentType type, double amount) throws WrongInputsException {
        Payment payment = new Payment(table, type, amount);
        if (table < 1 || table > 8)
            throw new WrongInputsException(TABLE_ERROR);
        if (type != PaymentType.Card && type != PaymentType.Cash)
            throw new WrongInputsException(TYPE_ERROR);
        if (amount <= 0)
            throw new WrongInputsException(AMOUNT_ERROR);

        payRepo.add(payment);
    }

    public double getTotalAmount(PaymentType type) throws WrongInputsException {
        if (type == null)
            throw new WrongInputsException(TYPE_ERROR);

        double total = 0.0f;    // 1
        List<Payment> paymentList = getPayments();  //1

        if (paymentList == null)    //2
            return total;

        if (paymentList.isEmpty())   //3
            return total;

        for (Payment payment : paymentList) {       // 4
            if (payment.getType() == null)           // 5
                continue;

            if (payment.getType().equals(type))     // 6
                total += payment.getAmount();       // 7
        }

        return total;       // 8
    }

}