package pizzashop.exceptions;

public class WrongInputsException extends Exception {
    public WrongInputsException(String errorMessage) {
        super(errorMessage);
    }
}
